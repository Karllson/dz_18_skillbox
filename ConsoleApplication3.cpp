﻿#include <iostream>
#include <deque>

using namespace std;

template <typename T>

class Stack
{
public:
	void push(T);
	T pop();
	void top();
	T emptu();
private:
	deque <T> a;
};

int main()
{
	Stack <int> f;
	f.push(1);
	f.push(2);
	f.push(3);
	cout << endl;
	f.top();
	cout << endl;
	f.pop();
	f.top();

}

template<class T> void Stack <T>::push(T data)
{
	a.push_back(data);
}

template<class T> T Stack <T>::pop()
{
	T data = a.back();
	a.pop_back();
	return data;
}

template<class T> void Stack <T>::top()
{
	for (auto d : a) cout << d << endl;;
}